#!/bin/bash

cat << EOM
--------------------------------------------------------------------------------

This device does not have a u-boot setup script in place.  You should refer
to the U-Boot user's guide provided with your SDK for details on how to
configure U-Boot.

--------------------------------------------------------------------------------
EOM
