#!/bin/bash

# This distribution contains contributions or derivatives under copyright
# as follows:
#
# Copyright (c) 2010, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# - Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# - Neither the name of Texas Instruments nor the names of its
#   contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cwd=`dirname $0`
. $cwd/common.sh

echo
echo "--------------------------------------------------------------------------------"
echo "This step will set up the u-boot variables for booting the EVM."

ipdefault=`ip addr show | grep 'inet ' | grep -v '127.0.0.1' | cut -d/ -f1 | awk '{ print $2 }'`
platform=`grep PLATFORM= $cwd/../Rules.make | cut -d= -f2`
prompt="$prompt"

echo "Autodetected the following ip address of your host, correct it if necessary"
read -p "[ $ipdefault ] " ip
echo

if [ ! -n "$ip" ]; then
    ip=$ipdefault
fi

if [ -f $cwd/../.targetfs ]; then
    rootpath=`cat $cwd/../.targetfs`
else
    echo "Where is your target filesystem extracted?"
    read -p "[ ${HOME}/targetNFS ]" rootpath

    if [ ! -n "$rootpath" ]; then
        rootpath="${HOME}/targetNFS"
    fi
    echo
fi

uimage="zImage-""$platform"".bin"
uimagesrc=`ls -1 $cwd/../board-support/prebuilt-images/$uimage`
uimagedefault=`basename $uimagesrc`

baseargs="console=ttyO2,115200n8 rw noinitrd"
optargs="vram=16M"
fssdargs="root=/dev/mmcblk0p2 rootfstype=ext4 rootwait"
fsnfsargs="root=/dev/nfs nfsroot=$ip:$rootpath,nolock,rsize=1024,wsize=1024 rootwait"

cat << EOM
For this device SD card is the only currently supported kernel location.
The kernel uImage should be placed in the "/boot" partition of the root
file system partition along with the device tree DTB files to be used
during boot.

EOM

read -p "Press Enter to Continue: " temp

echo
echo "Select root file system location:"
echo " 1: NFS"
echo " 2: SD card"
echo
read -p "[ 1 ] " fs

if [ ! -n "$fs" ]; then
    fs="1"
fi

if [ "$fs" -eq "1" ]; then
    bootargs="setenv bootargs $baseargs $optargs $fsnfsargs ip=dhcp"
    bootcmd="setenv bootcmd 'run findfdt; run loadimage; run loadfdt; bootm \${loadaddr} - \${fdtaddr}'"
    cfg="uimage-sd_fs-nfs"
else
    bootcmd="setenv bootcmd 'run findfdt; run loadimage; run loadfdt; run mmcboot'"
    cfg="uimage-sd_fs-sd"
fi

do_expect() {
    echo "expect {" >> $3
    check_status
    echo "    $1" >> $3
    check_status
    echo "}" >> $3
    check_status
    echo $2 >> $3
    check_status
    echo >> $3
}

echo
echo "--------------------------------------------------------------------------------"
echo "Would you like to create a minicom script with the above parameters (y/n)?"
read -p "[ y ] " minicom
echo

if [ ! -n "$minicom" ]; then
    minicom="y"
fi

#Use = instead of == for POSIX compliance and dash compatibility
if [ "$minicom" = "y" ]; then
    minicomfile=setup_$platform_$cfg.minicom
    minicomfilepath=$cwd/../$minicomfile

    if [ -f $minicomfilepath ]; then
        echo "Moving existing $minicomfile to $minicomfile.old"
        mv $minicomfilepath $minicomfilepath.old
        check_status
    fi

    timeout=300
    echo "timeout $timeout" >> $minicomfilepath
    echo "verbose on" >> $minicomfilepath
    echo >> $minicomfilepath
    do_expect "\"stop autoboot:\"" "send \"\"" $minicomfilepath
    do_expect "\"$prompt\"" "send \"env default -f -a\"" $minicomfilepath
    do_expect "\"$prompt\"" "send \"saveenv\"" $minicomfilepath
    do_expect "\"$prompt\"" "send \"reset\"" $minicomfilepath
    do_expect "\"stop autoboot:\"" "send \" \"" $minicomfilepath

    do_expect "\"$prompt\"" "send \"setenv bootdelay 3\"" $minicomfilepath
    do_expect "\"$prompt\"" "send \"setenv baudrate 115200\"" $minicomfilepath
#    do_expect "\"ENTER ...\"" "send \"\"" $minicomfilepath

#    echo "expect {" >> $minicomfilepath
#    check_status
#    echo "    \"$prompt\"" >> $minicomfilepath
#    check_status
#    echo "}" >> $minicomfilepath
#    check_status

    if [ -n "$bootargs" ]
    then
        #do_expect "\"$prompt\"" "send \"$bootargs\c\"\n" $minicomfilepath
        do_expect "\"$prompt\"" "send \"$bootargs\"" $minicomfilepath
    fi

    if [ -n "$bootcmd" ]
    then
        #do_expect "\"$prompt\"" "send \"$bootcmd\c\"\n" $minicomfilepath
        do_expect "\"$prompt\"" "send \"$bootcmd\"" $minicomfilepath
    fi

    do_expect "\"$prompt\"" "send \"saveenv\"" $minicomfilepath
    do_expect "\"$prompt\"" "! killall -s SIGHUP minicom" $minicomfilepath

    echo -n "Successfully wrote "
    readlink -m $minicomfilepath

set_ftdi_serial_port 1

cat << EOM

Would you like to run the setup script now (y/n)? This requires you to connect
the RS-232 cable between your host and EVM as well as your ethernet cable as
described in the Quick Start Guide. Once answering 'y' on the prompt below
you will have $timeout seconds to connect the board and power cycle it
before the setup times out

After successfully executing this script, your EVM will be set up. You will be 
able to connect to it by executing 'minicom -w' or if you prefer a windows host
you can set up Tera Term as explained in the Software Developer's Guide.
If you connect minicom or Tera Term and power cycle the board Linux will boot.

EOM
    read -p "[ y ] " minicomsetup

    if [ ! -n "$minicomsetup" ]; then
        minicomsetup="y"
    fi

    savedir=""
    if [ "$minicomsetup" = "y" ]; then
        savedir=$cwd
        cd "$cwd/.."
        check_status
        sudo minicom -S $minicomfile
        cd $savedir
        check_status
    fi

    echo "You can manually run minicom in the future with this setup script using: minicom -S $minicomfile"
fi
echo "--------------------------------------------------------------------------------"
